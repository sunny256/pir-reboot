Viser til [mail sendt ut 24. februar](mail.2017-02-24.gorm-hanssen.txt) til de fleste (?) ikke-suspenderte medlemmer.
Der kan vi bl.a. lese at sentralstyret nekter å kalle inn til ekstraordinært landsmøte.
Det skrives også «hele partiet bør ta lærdom av det som har inntruffet; påstander er ikke det samme som fakta».
Det meste her er fakta.

## Piratkodeks og kjerneprogram

Her kommer noen utdrag fra [Piratkodeksen](https://www.piratpartiet.no/kjerneprogrammet/politikken-fra-a-til-a/piratkodeksen/) og [kjerneprogrammet](https://www.piratpartiet.no/kjerneprogrammet/):

* Pirater misliker blind lydighet.
* Pirater lærer av sine feil.
* Pirater er rettferdige
* Pirater ivrer etter kunnskap
* Piratpartiet vil etablere demokratiske spilleregler som er tilpasset informasjonsalderen.
* Piratpartiet vil at borgere skal ha tilgang til all relevant informasjon for å kunne ta informerte beslutninger, og for å utføre nødvendig tilsyn med statsadministrasjonen.
* Tidligere avgjørelser gjort av Piratpartiet er alltid åpne for endringer.
* En enkeltpersons rett til å være informert skal aldri forhindres.

Etterlever styre og sekretariat disse punktene?

## Konkrete anklager

Sentralstyret og sekretariatet burde vise litt ydmykhet når rundt 20% av medlemmene bekrefter mistilitt.  I stedet ser vi at kritikken bare avfeies som "grunnløse påstander".  Dette er ikke løse påstander, dette er fakta:

* Sentralstyret har valgt å suspendere Øyvind A. Holm, Tobias Brox, Ståle Olsen og Øyvind Nondal.
  Dette regnes som fakta og er dokumentert i [sentralstyrereferat](https://wiki.piratpartiet.no/index.php?title=Referat_fra_sentralstyrem%C3%B8te_2017-02-17&rcid=7497).
    * Grunnlaget til suspensjonene er at de suspenderte motarbeider styret gjennom å oppfordre folk til å kreve ekstraordinært landsmøte.
      Dette har vi full anledning til ihht vedtektene.
      Mistillit til sittende styre er ikke gyldig eksklusjonsgrunnlag ihht vedtektene; vi ønsker ikke å *motarbeide* styre og sekretæriat, vi ønsker *å få byttet ut* styre og sekretæriat.
    * Suspensjonene gjør det vanskelig å få stilt lister i Oslo, Hordaland, samt Sogn- og Fjordane.
    * Mange av de som krever ekstraordinært landsmøte gjør det pga suspensjonene.
    * Sentralstyret har ikke revurdert sitt ståsted, men vil *vurdere* å *midlertidig* oppheve suspensjonene dersom vi ringer generalsekretæren og utfører arbeid på hans instrukser, ref [møtereferat](https://wiki.piratpartiet.no/index.php?title=Referat_fra_sentralstyrem%C3%B8te_2017-02-23) og [mail sendt til de suspenderte](mail.2017-02-24.gorm-hanssen.mulig-oppheving-av-suspensjon.txt).  Det er ikke akseptabelt.
* Sekretariatet nekter å opplyse om hvor mange medlemmer partiet har, ref [mailtråd mellom generalsekretær og Nondal](mail.2017-02-22.anders-kleppe.txt).
* Sentralstyret nekter å rette seg etter et legitimt krav om ekstraordinært landsmøte.
* Vanlige medlemmer og tillitsvalgte utenom sentralstyret har svært begrenset innsynsrett i partiets økonomiske disposisjoner.

## Vedtekter og ekstraordinært landsmøte

Adgangen til å kreve ekstraordinært landsmøte er dekket i [vedtektene](https://www.piratpartiet.no/partiet/vedtekter/), §7.2.
«Sentralstyret skal med minst 3 ukers varsel innkalle til ekstraordinært landsmøte, hvis 20% av medlemmene skriftlig anmoder styret om dette eller hvis minst halvparten av fylkesstyrene krever det».
Det står ingenting om formkrav eller forbehold til en slik anmodning; dersom 20% har skrevet at de støtter et krav om ekstraordinært landsmøte, så skal det kalles inn til ekstraordinært landsmøte.
Sentralstyret avslår et slikt krav med begrunnelse «betingelsene for et slikt krav er ikke oppfylt», samt at påstandene som blir fremmet er grunnløse.

Det er ingen åpenhet om hvor mange som har krevd ekstraordinært landsmøte, eller hvor mange betalende medlemmer piratpartiet har.
Sekretariatet har fått spørsmål, og generalsekretæren responderer med at spørsmålet vil bli besvart på neste ordinære landsmøte.
Vi tror at 22 medlemmer har krevd ekstraordinært landsmøte; det er 20% av 110.  Vi tror at antall betalende medlemmer ligger en god del lavere, kanskje rundt 60-70.  Kravet om ekstraordinært landsmøte er i såfall absolutt, unnlatelse er i strid med vedtektene.

## Økonomi

Regnskapet for 2014 og 2015 ligger nå [åpent ute på nett](https://wiki.piratpartiet.no/images/c/c1/%C3%85rsregnskap_2015); disse ble lastet opp 24. februar til wikien.
Fakta: én million kroner har gått med på «annen driftskostnad» i 2015.
Mer detaljer enn dette er ikke tilgjengelig for det allminnelige medlem.
Hadde detaljene vært åpent tilgjengelig, eller om vi hadde hatt noe vis å kontrollere det på, så hadde vi ikke hatt behov for å forholde oss til løse påstander og rykter.

Det ryktes at omlag hundre tusen har gått til å bestille varer og tjenester fra enkeltmannsforetaket «GC TRADE ANDERS KLEPPE» i 2016.
Det behøver ikke være noe galt i dette, men det må være full åpenhet om slikt.

Det ryktes at det i 2015 ble brukt kroner 96866 på bevertning, 176075 på reiser, 81550 på telefoni og 66618 kroner på andre kontorkostnader.
Er ikke det ganske høyt?
Kan det forsvares?
At disse tallene er godkjent av revisor er i så måte helt irrelevant, revisor kontrollerer at bilagene stemmer, revisor kan ikke sjekke om forbruket var hensiktsmessig.
2015 var et valgår, men hvor store berettigede kostnader har vi hatt i så måte?
Vi hadde en del kostnader ifbm sabotasje av valgbod og diverse i Oslo, Tobias tok en del av kostnadene fra egen lomme da han visste at piratpartiet var i en vanskelig økonomisk situasjon.
Det ryktes at kostnader til bevertning etc inkluderer et lederseminar i januar 2015 som kostet 91694.98.

Har piratpartiet hatt noen som helst nytte av den millionen som gikk med til «annen driftskostnad» i 2015?
Piratpartiet var langt mer vitalt i 2013, da vi ikke hadde signifikante inntekter.

Om forbruket har vært hensiktsmessig eller ikke er uansett av underordnet betydning, poenget mitt er at vi burde hatt full åpenhet om økonomien fra dag én.
I kjølvannet av underslagssaken ble det lovet at man skulle få nye og bedre rutiner på økonomi, men er det gjort noe som helst endringer?
Finnes det andre enn generalsekretæren som har full kontroll og innsyn over økonomien?
